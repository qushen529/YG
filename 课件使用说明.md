


## 课件使用说明 (因果推断)

> &#x1F34E; 这是「因果推断」专题课程的课件使用说明网页版，隐藏了课件下载地址。   
> &ensp; &ensp; 完整版课件使用说明以课程群中收到的【课件使用说明-因果推断-连享会.pdf】为准。

&emsp;

> Stata 连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html) || [视频](http://lianxh.duanshu.com) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

> [听课建议](https://www.lianxh.cn/news/69706e871c9ad.html) || [Stata资源链接](https://www.lianxh.cn/news/46917f1076104.html) || [Stata Journal](https://www.lianxh.cn/news/12ffe67d8d8fb.html) || [推文分类](https://www.lianxh.cn/news/d4d5cd7220bc7.html)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp; 

> &#x1F353; 因果推断 · 课程主页：<https://gitee.com/arlionn/YG>  
> &ensp; &ensp; 温馨提示：本课程的补充资料和 FAQs 都通过该主页发布    
>   
> &#x1F332; 课程大纲 PDF 版：[下载](https://www.jianguoyun.com/p/Dbh53SkQtKiFCBjxj8sD)


&emsp;

## 1. 电子课件下载

本次课程的所有电子课件统一存放于 **YG.rar**。软件下载参见 **第 3 节** 。

> 下载建议：注册一个 [坚果云](https://www.jianguoyun.com/) 账号，速度会更快一些


&emsp;

--- - --

### 1.1 下载  **YG.rar** 压缩包

- 坚果云：xxxx
- 密码：xxxx

--- - --


&emsp;

### 1.2 课件使用方法-Windows 用户

**a. 课件存放：** 将 **YG.rar** 解压后放置于 【D:\Lianxh】 文件夹下即可。注意事项如下：

  - 若没有 **Lianxh** 子文件夹，可以自行新建一个，注意：**Lianxh** 首字母大写。
  - 最终目录为：【D:\Lianxh\YG】，而不是【D:\Lianxh\YG\YG】。

**b. 没有 D 盘怎么办？** 按如下步骤：

1. 在其他盘符下新建【Lianxh】文件夹，如【**C:\Lianxh**】
2. 将 **YG.rar** 解压后放置于 【C:\Lianxh】 
3. (重要！) 修改 dofile。以 **A2_OLS.do** 为例，在命令窗口中输入 `doedit C:\Lianxh\YG\A2_OLS.do` 打开第二讲的 dofile，然后将 **第 #51 行** `global mypath "D:\Lianxh\YG"` 修改为 `global mypath "C:\Lianxh\YG"` 即可 (即，`D:\` 改为 `C:\`)。

按照上述方法，可以进一步修改 **A3_IV.do**，**A4_RDD.do** 等文档中的对应语句。其实，你可以把 【**YG**】放在任何位置，只需在 dofile 中修改它所在的路径即可。  

&emsp;

### 1.3 课件使用方法-Mac 用户

> Note：Windows 用户可以略过本节内容。

**c. Mac - 课件存放** 按如下步骤：

1. 在 Stata 安装路径的【personal】文件夹下 (如 `/Applications/Stata/ado/personal`) 下新建【Lianxh】文件夹
2. 将 **YG.rar** 解压后放置于【Lianxh】文件夹下 
3. 课件中同时提供了 Mac 版的 do 文档，我们已经设置路径为 `global mypath "c(sysdir_personal)'/Lianxh/YG"` ，如果你将课件放置在其他位置，比如你没有新建【Lianxh】文件夹，而是直接放置在【personal】文件夹下，那么只需将路径改为 `global mypath"c(sysdir_personal)'/YG"` 即可。

**d. Mac - 找不到【personal】和【plus】文件夹怎么办？** 按如下步骤：

1. 在命令窗口输入命令 `sysdir`,

   若结果显示【`PLUS:/Applications/Stata/ado/plus/`】以及
   【`PERSONAL:  /Applications/Stata/ado/personal/`】，则没有问题，可以按 c 步骤进行。 

2. 若结果显示【personal】和【plus】并不在上述路径下，则可以下载坚果云链接中提供的**附件**压缩包，解压后找到**Mac**版，按如下步骤进行：

  - 将我们提供的 **profile.do** 放置在 Stata 文件夹根目录下。
  - 将【plus】压缩包解压到【ado】文件夹下。
  - 在【ado】文件夹下新建【personal】文件夹。
  - 在【personal】文件夹下新建 **Lianxh** 文件，将 **YG.rar** 解压后放置在 **Lianxh** 文件夹下。
  - 重启 Stata 软件，此时 `profile.do` 文件会自动运行，并设置好路径。

  注：`profile.do` 文件中可以设置回归系数、p 值以及标准误等的默认显示格式，你可以自行修改默认小数点显示位数，比如将回归系数设置为小数点后四位，可以将 `set cformat  %4.3f` 改为 `set cformat  %5.4f` 即可。另外，还可以设置 Stata 默认的工作路径，我们已在 profile.do 里设置默认工作路径为 `cd` "``c(sysdir_personal)'/Lianxh/YG"``，你也可以自行修改默认工作路径。

- **苹果用户扩展阅读：** 有关 Mac 系统的其它 Stata 设定，参见下文：(遇到困难时再读，基本上不需要了)
  - [苹果用户：Stata for Mac 使用指南](https://www.lianxh.cn/news/a7813df2d77d6.html)
  - [Stata: 苹果(MAC)用户无法使用 shellout 命令？](https://www.lianxh.cn/news/cb8aedcbeac20.html)

&emsp; 

## 2. 课件使用
- **幻灯片讲义**：存放于【**D:\Lianxh\YG\_Lectures**】，请预先自行打印，以便上课时记录。
- **Stata dofile**：存放于【**D:\Lianxh\YG**】目录下，如 `A2_OLS.do` 对应第二讲的 Stata 实操 dofile，【A2_OLS】文件夹中存放了本讲所需范例数据等文件，可以不必打开查看。
  - **打开方式 1**：在 Stata 命令窗口中输入 `cd D:\Lianxh\YG`，进入课程目录，进而输入 `doedit A1_OLS.do` 即可打开；
  - **打开方式 2**：在 Stata 命令窗口中输入 `doedit D:\Lianxh\YG\A2_OLS.do` 即可直接打开；
  - Note：若你的课间存放路径自行变更了，则酌情修改上面提到的路径即可。



&emsp;

## 3. 软件

- **版本：** Stata 15.0 以上版本即可。
- **特别提醒：** 本次课程的课件已经内嵌了所有课程中用到的外部命令，无需单独下载。使用你自己的 Stata15.0 以上版本的软件也没有问题。 
- **外部命令：** 若使用自己的 Stata 软件，想获得更多的外部命令支持，可以到 <https://gitee.com/arlionn/StataPlus> 主页下载。该操作与本次课程无关。


&emsp;

## 4. 答疑和课程建议

本次课程共安排了 13 位经验丰富的助教，与大家共同交流、进步。大家提问时，可以在群里 `@助教` 以便及时获得回应。

### 提问须知
- **温馨提示：** 由于精力有限，与课程内容无关的问题仅在助教们力所能及的情况下给与回复，望见谅。
- 大家可以在课程微信群中就课程中涉及的问题提问，提问前可以先到 [连享会问答区](https://gitee.com/arlionn/WD) 和 [连享会暑期班 FAQs](https://gitee.com/arlionn/PX/wikis/%E8%BF%9E%E7%8E%89%E5%90%9B-Stata%E5%AD%A6%E4%B9%A0%E5%92%8C%E5%90%AC%E8%AF%BE%E5%BB%BA%E8%AE%AE.md?sort_id=2662737) 查看是否已经有相似的问题。亦可到 [连享会推文集锦](https://www.lianxh.cn/news/d4d5cd7220bc7.html) 页面，输入 `Ctrl+F` 快捷键搜索关键词，查看相应推文。搜索问题的过程，也是有效学习的过程。 
- 若上述途径都无法找到合适的解答，请详述你的问题背景，提出问题。比如，你要介绍是第几讲第几页或第几行讲义出现的问题，你的疑虑或错误信息是什么 (可以配合截图，微信截图快捷键为 `Alt+A`)。

### 答疑时间安排

- 课前：8:00-8:50 助教答疑时段
- 课后 1：下午 17:30-18:00 为主讲老师答疑时段
- 课后 2：下午 19:30-22:00 为助教答疑时段
- 每天的答疑汇总文档会发布于 [因果推断-FAQs](https://gitee.com/arlionn/YG/wikis/Home?sort_id=3098274) 页面

### 助教小组分工 

- **Day1**：张德亮 (组长)   王洪鹏 杨继超 杜思佳
- **Day2**：徐云娇 (组长)   秦利宾 甘徐沁
- **Day3**：涂漫漫 (组长)   刘雅玄 杨雅程
- **Day4**：孙碧洋 (组长)   苗妙 孙爱晶





&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

> [听课建议](https://www.lianxh.cn/news/69706e871c9ad.html) || [Stata资源链接](https://www.lianxh.cn/news/46917f1076104.html) || [Stata Journal](https://www.lianxh.cn/news/12ffe67d8d8fb.html) || [推文分类](https://www.lianxh.cn/news/d4d5cd7220bc7.html)











